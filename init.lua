minetest.register_node("lightnode:airlight", {
    description = "Air Light",
    drawtype = "airlike",
    paramtype = "light",
    sunlight_propagates = true,

    light_source = 7,

    walkable     = false, -- Would make the player collide with the air node
    pointable    = false, -- You can't select the node
    diggable     = false, -- You can't dig the node
    buildable_to = true,  -- Nodes replace this node.
                          -- (you can place a node and remove the air node
                          -- that used to be there)

    air_equivalent = true,
    drop = "",
    groups = {not_in_creative_inventory=1}
})